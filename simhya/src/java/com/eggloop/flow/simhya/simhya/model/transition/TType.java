/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eggloop.flow.simhya.simhya.model.transition;

/**
 *
 * @author Luca
 */
public enum TType {
    STOCHASTIC, CONTINUOUS, HYBRID, INSTANTANEOUS, TIMED
}
