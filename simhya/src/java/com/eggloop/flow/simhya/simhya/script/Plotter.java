/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eggloop.flow.simhya.simhya.script;

/**
 *
 * @author luca
 */
public interface Plotter {

    public String plot() throws ScriptException;

}
