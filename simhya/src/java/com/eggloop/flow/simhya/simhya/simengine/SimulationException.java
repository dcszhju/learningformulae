/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eggloop.flow.simhya.simhya.simengine;

/**
 *
 * @author Luca
 */
public class SimulationException extends RuntimeException {

    public SimulationException(String message) {
        super(message);
    }
    
}
