package com.eggloop.flow.simhya.simhya.simengine.odeevents;

import com.eggloop.flow.simhya.simhya.simengine.ProgressMonitor;

public interface PrintableEvent {
    void setPrintEvent(ProgressMonitor var1);

}